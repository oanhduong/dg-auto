const element = require('../../support/element')
const path = require('path')
const util = require('../../support/util')

const track = (id, data) => {
  cy.writeFile(path.join('data', `${id}.track`), `${data}\n`)
}

const json = (id, data) => {
  cy.writeFile(path.join('data', `${id}.json`), `${JSON.stringify(data)}\n`)
}

const cookies = (id, data) => {
  cy.writeFile(path.join('data', `${id}.cookies`), `${JSON.stringify(data)}\n`)
}

const waitForCode = (id) => {
  return cy.readFile(path.join('data', `${id}.json`)).then((data) => {
    if (data && data.code) {
      track(id, 'GOT_CODE')
      cy.get(element.fb.facode).type(String(data.code).trim())
      return Promise.resolve(data)
    } else {
      cy.wait(2000)
      return waitForCode(id)
    }
  })
}

const waitForUserPass = (id) => {
  return cy.readFile(path.join('data', `${id}.json`)).then((data) => {
    if (data && data.username && data.password) {
      track(id, 'GOT_USER_PASS')
      cy.get(element.fb.username).type(data.username)
      cy.get(element.fb.password).type(data.password)
      cy.get(element.fb.login).click()
      return Promise.resolve(data)
    } else {
      cy.wait(1000)
      return waitForUserPass(id)
    }
  })
}

const autoContinue = (el) => new Promise((res) => {
  cy.url()
    .then((url) => {
      if (url.includes('checkpoint')) {
        cy.get(el).click()
        res(autoContinue(el))
      } else {
        res()
      }
    })
})

const checkCases = (id) => new Promise((res) => {
  cy.url()
    .then((url) => {
      if (url.includes('login_attempt')) {
        util.isInputExist(element.fb.username)
          .then((txt) => {
            if (txt) {
              return Promise.resolve()
            } else {
              return Promise.reject(track(id, 'ERROR_USER'))
            }
          })
          .then(() => util.isInputExist(element.fb.password))
          .then((text) => res(text ? null : track(id, 'ERROR_PASS')))
          .catch(() => { })

      } else if (url.includes('checkpoint')) {
        track(id, 'WAIT_FOR_CODE')
        waitForCode(id)
        autoContinue(element.fb.continue)
      } else {
        res(true)
      }
    })
})

describe('Login FB ...', () => {

  let username = ''
  let password = ''
  const id = Cypress.env('ID')

  before(() => {
    cy.visit('/login', {
      timeout: 30000,
    })

    json(id, { username, password, code: '' })
    cookies(id, {})
    track(id, '')

    waitForUserPass(id)
  })

  it('Visits the BF link', () => {

    checkCases(id)

  })

  after(() => {
    track(id, 'LOGIN_SUCCESS')
    cy.readFile(path.join('data', `${id}.json`))
      .then((data) => {
        data.code = ''
        json(id, data)
      })
    cy.getCookies().then((ck) => cookies(id, ck))
  })
})
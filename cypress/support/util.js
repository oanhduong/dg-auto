const element = require('./element')

const isInputExist = (el) => new Promise((res, rej) => {
    cy.get(el).invoke('val').then((txt) => res(txt))
})

const clickIfExist = (el) => new Promise((res) => {
    cv.get(el).click().then(() => res(true)).catch(() => rej())
})

module.exports = {isInputExist, clickIfExist }

